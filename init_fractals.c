/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_fractals.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/14 10:24:41 by nbelouni          #+#    #+#             */
/*   Updated: 2015/01/27 19:12:01 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

int			init_julia_celtic(t_env *e)
{
	e->s.x1 = -1.6;
	e->s.x2 = 1.6;
	e->s.y1 = -1.9;
	e->s.y2 = 1.9;
	e->m.x = -0.0;
	e->m.y = 0.0;
	e->s.zoom = 1;
	e->f.r = 0.95;
	e->f.i = -0.75;
	e->s.iter_max = 100;
	return (0);
}

int			init_celtic(t_env *e)
{
	e->s.x1 = -2.1;
	e->s.x2 = 3.6;
	e->s.y1 = -1.7;
	e->s.y2 = 1.7;
	e->m.x = -0.5;
	e->m.y = 0.1;
	e->s.zoom = 1;
	e->f.r = 0.0;
	e->f.i = 0.0;
	e->s.iter_max = 100;
	return (0);
}

int			init_julia(t_env *e)
{
	e->s.x1 = -1.6;
	e->s.x2 = 1.6;
	e->s.y1 = -1.9;
	e->s.y2 = 1.9;
	e->m.x = 0.0;
	e->m.y = 0.0;
	e->s.zoom = 1;
	e->f.r = 0.0;
	e->f.i = 0.0;
	e->s.iter_max = 50;
	return (0);
}

int			init_bird(t_env *e)
{
	e->s.x2 = 2.1;
	e->s.x1 = -3.1;
	e->s.y2 = 2.2;
	e->s.y1 = -1.8;
	e->m.x = 0.0;
	e->m.y = 0.0;
	e->f.r = 0.0;
	e->f.i = 0.0;
	e->s.zoom = 1;
	e->s.iter_max = 50;
	return (0);
}

int			init_mandelbrot(t_env *e)
{
	e->s.x2 = 0.6;
	e->s.x1 = -2.1;
	e->s.y2 = 1.2;
	e->s.y1 = -1.2;
	e->m.x = 0.0;
	e->m.y = 0.0;
	e->f.r = 0.0;
	e->f.i = 0.0;
	e->s.zoom = 1;
	e->s.iter_max = 50;
	return (0);
}
