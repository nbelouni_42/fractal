/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot_fractal.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/10 00:59:53 by nbelouni          #+#    #+#             */
/*   Updated: 2014/12/22 11:38:05 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

inline int			algo(t_env *e, t_form c)
{
	long double	tmp;

	tmp = e->f.r;
	e->f.r = e->f.r * e->f.r - e->f.i * e->f.i + c.r;
	e->f.i = 2 * e->f.i * tmp + c.i;
	return (0);
}

inline long double	less(int i, t_env *e, int c)
{
	if (c == 'x')
		return ((i + e->s.zoom) / (e->s.x2 - e->s.x1));
	return ((i + e->s.zoom) / (e->s.y2 - e->s.y1));
}

inline void			init_f_m(t_form *s, t_form *tp, t_env *e)
{
	tp->r = e->f.r;
	tp->i = e->f.i;
	s->r = (600 + e->s.zoom) / (e->s.x2 - e->s.x1);
	s->i = (425 + e->s.zoom) / (e->s.y2 - e->s.y1);
}

int					mandelbrot_fractal(t_env *e, t_pixel *p)
{
	t_form		c;
	t_form		tp;
	t_form		s;
	int			i;

	init_f_m(&s, &tp, e);
	while (p->x < 600)
	{
		p->y = 0;
		while (p->y < 425)
		{
			c.r = p->x / s.r + e->s.x1 + e->m.x;
			c.i = p->y / s.i + e->s.y1 + e->m.y;
			e->f.r = tp.r;
			e->f.i = tp.i;
			i = algo(e, c);
			while ((e->f.r * e->f.r + e->f.i * e->f.i) < 4 && i < e->s.iter_max)
				i = i + 1 + algo(e, c);
			p->y = p->y + 1 + magic_color(p, i, e, c.i);
		}
		p->x++;
	}
	e->f.r = tp.r;
	e->f.i = tp.i;
	return (0);
}
