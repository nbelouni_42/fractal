/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fract_ol.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/09 18:26:04 by nbelouni          #+#    #+#             */
/*   Updated: 2014/12/21 15:09:18 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACT_OL_H
# define FRACT_OL_H
# include <mlx.h>
# include <stdlib.h>
# include <unistd.h>
# include <libft.h>

typedef struct	s_form
{
	long double	r;
	long double	i;
}				t_form;

typedef struct	s_size
{
	long double	zoom;
	long double	h;
	long double	x1;
	long double	x2;
	long double	y1;
	long double	y2;
	int			iter_max;
}				t_size;

typedef struct	s_move
{
	long double	x;
	long double	y;
}				t_move;

typedef struct	s_env
{
	void	*mlx;
	void	*win;
	void	*img;
	int		x;
	int		y;
	char	julia;
	char	mandelbrot;
	char	celtic;
	char	julia_celtic;
	char	bird;
	char	motion;
	char	fractal_zoom;
	t_form	f;
	t_size	s;
	t_move	m;
}				t_env;

typedef struct	s_pixel
{
	int x;
	int y;
	int color;
}				t_pixel;

int				put_pixel_on_image(t_env *env, t_pixel *p);
int				mandelbrot_fractal(t_env *e, t_pixel *p);
int				julia_fractal(t_env *e, t_pixel *p);
int				celtic_fractal(t_env *e, t_pixel *p);
int				julia_celtic_fractal(t_env *e, t_pixel *p);
int				bird_fractal(t_env *e, t_pixel *p);
int				mouse(int x, int y, t_env *env);
int				mouse_hook(int button, int x, int y, t_env *env);
int				magic_color(t_pixel *p, int i, t_env *e, long double c);
int				celtic_color(t_env *e, t_pixel *p, int i);
int				julia_color(t_env *e, t_pixel *p, int i);
int				julia_celtic_color(t_env *e, t_pixel *p, int i);
int				bird_color(t_env *e, t_pixel *p, int i);
int				init_bird(t_env *e);
int				init_julia_celtic(t_env *e);
int				init_celtic(t_env *e);
int				init_julia(t_env *e);
int				init_mandelbrot(t_env *e);
int				init_env(t_env *e);
void			print_julia(t_env *env, t_pixel *p);
void			print_mandelbrot(t_env *env, t_pixel *p);
void			print_celtic(t_env *env, t_pixel *p);
void			print_julia_celtic(t_env *env, t_pixel *p);
void			print_bird(t_env *env, t_pixel *p);
void			print_info(void);
int				check_arg(char *s, t_env *env);

#endif
