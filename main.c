/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/09 18:23:55 by nbelouni          #+#    #+#             */
/*   Updated: 2014/12/17 01:22:00 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

int			expose_hook(t_env *env)
{
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	return (0);
}

int			key_hook(int keycode, t_env *env)
{
	t_pixel	p;

	p.x = 0;
	p.y = 0;
	p.color = 0x000000;
	if (keycode == 65307)
		exit(0);
	if (keycode == 109)
		env->motion = (env->motion > 0) ? 0 : 1;
	if (keycode == 122)
		env->fractal_zoom = (env->fractal_zoom > 0) ? 0 : 1;
	if (keycode == 105)
		env->s.iter_max *= 1.5;
	if (keycode == 106)
		print_julia(env, &p);
	else if (keycode == 98)
		print_mandelbrot(env, &p);
	else if (keycode == 99)
		print_celtic(env, &p);
	else if (keycode == 120)
		print_julia_celtic(env, &p);
	else if (keycode == 112)
		print_bird(env, &p);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	return (0);
}

int			main(int argc, char **argv)
{
	t_env	env;

	if (init_env(&env))
		return (0);
	if (argc < 2)
		print_info();
	else
	{
		if (check_arg(argv[1], &env) == 1)
		{
			mlx_destroy_window(env.mlx, env.win);
			return (0);
		}
		mlx_mouse_hook(env.win, mouse_hook, &env);
		mlx_hook(env.win, 6, 1L << 6, mouse, &env);
		mlx_key_hook(env.win, key_hook, &env);
		mlx_expose_hook(env.win, expose_hook, &env);
		mlx_loop(env.mlx);
		mlx_destroy_image(env.mlx, env.img);
		mlx_destroy_window(env.mlx, env.win);
	}
	return (0);
}
