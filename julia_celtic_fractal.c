/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia_celtic_fractal.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/15 06:38:02 by nbelouni          #+#    #+#             */
/*   Updated: 2015/01/27 19:21:00 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

inline long double	ft_abs_jc(long double n)
{
	return ((n = (n >= 0) ? n : n * -1));
}

inline int			equation(t_form *z, t_form *c)
{
	long double		tmp;

	tmp = z->r;
	z->r = ft_abs_jc(z->r * z->r - z->i * z->i) - c->r;
	z->i = 2 * z->i * tmp - c->i;
	return (0);
}

int					julia_celtic_fractal(t_env *e, t_pixel *p)
{
	t_form		c;
	t_form		z;
	t_form		s;
	int			i;

	s.r = (600 + e->s.zoom) / (e->s.x2 - e->s.x1);
	s.i = (425 + e->s.zoom) / (e->s.y2 - e->s.y1);
	while (p->x < 600)
	{
		p->y = 0;
		while (p->y < 425)
		{
			c.r = 0.285 + e->f.r;
			c.i = 0.1 + e->f.i;
			z.r = p->x / s.r + e->s.x1 + e->m.x;
			z.i = p->y / s.i + e->s.y1 + e->m.y;
			i = equation(&z, &c);
			while (z.r * z.r + z.i * z.i < 4 && i < e->s.iter_max)
				i = i + 1 + equation(&z, &c);
			julia_celtic_color(e, p, i);
			p->y++;
		}
		p->x++;
	}
	return (0);
}
