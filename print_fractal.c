/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_fractal.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/15 07:53:20 by nbelouni          #+#    #+#             */
/*   Updated: 2014/12/16 10:24:58 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

void			print_julia(t_env *env, t_pixel *p)
{
	init_julia(env);
	julia_fractal(env, p);
	env->julia = 1;
	env->mandelbrot = 0;
	env->celtic = 0;
	env->julia_celtic = 0;
	env->bird = 0;
}

void			print_mandelbrot(t_env *env, t_pixel *p)
{
	init_mandelbrot(env);
	mandelbrot_fractal(env, p);
	env->mandelbrot = 1;
	env->julia = 0;
	env->celtic = 0;
	env->julia_celtic = 0;
	env->bird = 0;
}

void			print_celtic(t_env *env, t_pixel *p)
{
	init_celtic(env);
	celtic_fractal(env, p);
	env->celtic = 1;
	env->julia = 0;
	env->mandelbrot = 0;
	env->julia_celtic = 0;
	env->bird = 0;
}

void			print_julia_celtic(t_env *env, t_pixel *p)
{
	init_julia_celtic(env);
	julia_celtic_fractal(env, p);
	env->julia_celtic = 1;
	env->julia = 0;
	env->mandelbrot = 0;
	env->celtic = 0;
	env->bird = 0;
}

void			print_bird(t_env *env, t_pixel *p)
{
	init_bird(env);
	bird_fractal(env, p);
	env->bird = 1;
	env->julia = 0;
	env->mandelbrot = 0;
	env->celtic = 0;
	env->julia_celtic = 0;
}
