/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_image.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/15 04:31:13 by nbelouni          #+#    #+#             */
/*   Updated: 2015/02/18 13:39:52 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

int			magic_color(t_pixel *p, int i, t_env *e, long double c)
{
	if (i != e->s.iter_max)
		p->color = 0x666666;
	else
		p->color = 0x000000;
	p->color *= 0x9 * i / (e->s.iter_max + c);
	put_pixel_on_image(e, p);
	return (0);
}

int			celtic_color(t_env *e, t_pixel *p, int i)
{
	t_pixel	tmp;

	tmp.y = p->y;
	tmp.x = p->x;
	p->y = tmp.x;
	p->x = tmp.y;
	if (i == e->s.iter_max)
		p->color = 0xffffff;
	else
	{
		if (i == 0)
			i = 1;
		p->color = 0xf0f000 / i;
	}
	put_pixel_on_image(e, p);
	p->x = tmp.x;
	p->y = tmp.y;
	return (0);
}

int			julia_color(t_env *e, t_pixel *p, int i)
{
	if (i == e->s.iter_max)
		p->color = 0xff0000 * i / 70;
	else
		p->color = 0x000000;
	put_pixel_on_image(e, p);
	return (0);
}

int			bird_color(t_env *e, t_pixel *p, int i)
{
	if (i >= e->s.iter_max)
		p->color = 0xffffff;
	else
		p->color = 0xffff * i / 5;
	put_pixel_on_image(e, p);
	return (0);
}

int			julia_celtic_color(t_env *e, t_pixel *p, int i)
{
	if (i >= e->s.iter_max)
		p->color = 0xffffff;
	else
		p->color = 0xffffff * i / 5;
	put_pixel_on_image(e, p);
	return (0);
}
