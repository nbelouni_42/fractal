/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   celtic_fractal.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/10 00:59:53 by nbelouni          #+#    #+#             */
/*   Updated: 2014/12/21 15:07:36 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

inline long double	ft_abs(long double n)
{
	return ((n = (n >= 0) ? n : n * -1));
}

inline int			algo_bs(t_env *e, t_form c)
{
	long double		tmp;

	tmp = e->f.r;
	e->f.r = ft_abs(e->f.r * e->f.r - e->f.i * e->f.i) - c.r;
	e->f.i = 2 * e->f.i * tmp - c.i;
	return (0);
}

inline int			init_form(t_form *f, t_form *t, int *i)
{
	f->r = t->r;
	f->i = t->i;
	*i = -1;
	return (0);
}

inline void			init_s(t_form *s, t_form *tp, t_env *e)
{
	tp->r = e->f.r;
	tp->i = e->f.i;
	s->r = (600 + e->s.zoom) / (e->s.x2 - e->s.x1);
	s->i = (425 + e->s.zoom) / (e->s.y2 - e->s.y1);
}

int					celtic_fractal(t_env *e, t_pixel *p)
{
	t_form			c;
	t_form			tp;
	t_form			s;
	int				i;

	init_s(&s, &tp, e);
	while (p->x < 425)
	{
		p->y = 0;
		while (p->y < 600)
		{
			c.r = p->x / s.r + e->s.x1 + e->m.y;
			c.i = p->y / s.i + e->s.y1 + e->m.x;
			init_form(&(e->f), &tp, &i);
			algo_bs(e, c);
			while ((e->f.r * e->f.r + e->f.i * e->f.i) < 4
				&& ++i < e->s.iter_max)
				algo_bs(e, c);
			celtic_color(e, p, i);
			p->y++;
		}
		p->x++;
	}
	init_form(&(e->f), &tp, &i);
	return (0);
}
