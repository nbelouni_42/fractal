/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bird_of_prey.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/15 04:33:33 by nbelouni          #+#    #+#             */
/*   Updated: 2014/12/21 15:06:43 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

inline long double	ft_abs_bp(long double n)
{
	return ((n = (n >= 0) ? n : n * -1));
}

inline int			algo_bp(t_env *e, t_form c)
{
	long double	tmp;

	tmp = e->f.r;
	e->f.r = (tmp * tmp - (e->f.i * e->f.i * 3)) * ft_abs_bp(tmp) + c.r;
	e->f.i = ((tmp * tmp * 3) - e->f.i * e->f.i) * ft_abs_bp(e->f.i) + c.i;
	return (0);
}

inline void			init_f_bp(t_form *s, t_form *tp, t_env *e)
{
	tp->r = e->f.r;
	tp->i = e->f.i;
	s->r = (600 + e->s.zoom) / (e->s.x2 - e->s.x1);
	s->i = (425 + e->s.zoom) / (e->s.y2 - e->s.y1);
}

int					bird_fractal(t_env *e, t_pixel *p)
{
	t_form		c;
	t_form		tp;
	t_form		s;
	int			i;

	init_f_bp(&s, &tp, e);
	while (p->x < 600)
	{
		p->y = 0;
		while (p->y < 425)
		{
			c.r = p->x / s.r + e->s.x1 + e->m.x;
			c.i = p->y / s.i + e->s.y1 + e->m.y;
			e->f.r = tp.r;
			e->f.i = tp.i;
			i = algo_bp(e, c);
			while ((e->f.r * e->f.r + e->f.i * e->f.i) < 4 && i < e->s.iter_max)
				i = i + 1 + algo_bp(e, c);
			p->y = p->y + 1 + bird_color(e, p, i);
		}
		p->x++;
	}
	e->f.r = tp.r;
	e->f.i = tp.i;
	return (0);
}
