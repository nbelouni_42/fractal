/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_image.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/15 04:30:59 by nbelouni          #+#    #+#             */
/*   Updated: 2014/12/17 03:00:43 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

int			put_pixel_on_image(t_env *env, t_pixel *p)
{
	char	*data;
	int		i;
	int		bpp;
	int		sl;
	int		endian;

	data = mlx_get_data_addr(env->img, &bpp, &sl, &endian);
	bpp /= 8;
	i = p->x * bpp + p->y * sl;
	if (p->x >= 600 || p->y >= 425)
		return (0);
	data[i] = p->color;
	data[i + 1] = p->color >> 8;
	data[i + 2] = p->color >> 16;
	return (0);
}

void		print_info(void)
{
	ft_putstr("\n		Fractals :\n	-b : mandelbrot set\n");
	ft_putstr("	-j : julia set\n	-c : celtic mandelbrot\n");
	ft_putstr("	-x : celtic julia\n	-p : bird of prey\n\n");
	ft_putstr("		Options :\n	m : move\n	z : zoom\n");
	ft_putstr("	init fractal : argument fractal\n\n");
}
