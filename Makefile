# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/30 11:19:25 by nbelouni          #+#    #+#              #
#    Updated: 2014/12/23 08:13:03 by nbelouni         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME= fractol
CC= gcc
CFLAGS= -Wall -Werror -Wextra -O3
XFLAGS= -L /usr/X11/lib -lmlx -lxext -lX11
LFLAGS= -I libft/includes/
SRC= bird_of_prey.c celtic_fractal.c check_arg.c color_image.c init_env.c\
	 init_fractals.c julia_celtic_fractal.c julia_fractal.c main.c\
	 mandelbrot_fractal.c mouse.c print_fractal.c print_image.c
OBJ= bird_of_prey.o celtic_fractal.o check_arg.o color_image.o init_env.o\
	 init_fractals.o julia_celtic_fractal.o julia_fractal.o main.o\
	 mandelbrot_fractal.o mouse.o print_fractal.o print_image.o

all: $(NAME)

$(NAME): $(OBJ) 
	$(CC) $(XFLAGS) -o $(NAME) $(OBJ) -L libft -lft

$(OBJ): $(SRC)
	make -C libft
	$(CC) $(CFLAGS) $(XFLAGS) $(LFLAGS) -c $(SRC)

clean:
	rm -f $(OBJ)
	make -C libft clean

fclean: clean
	rm -f $(NAME)
	make -C libft fclean

re: clean all
