/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/12 03:00:26 by nbelouni          #+#    #+#             */
/*   Updated: 2015/02/14 17:54:17 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

int				prev_motion(int x, int y, t_env *env)
{
	if (env->x && env->y)
	{
		if (x > env->x)
			env->f.r += 0.01;
		else if (x < env->x)
			env->f.r -= 0.01;
		if (y > env->y)
			env->f.i += 0.01;
		else if (y < env->y)
			env->f.i -= 0.01;
	}
	env->x = x;
	env->y = y;
	return (0);
}

int				mouse_motion(int x, int y, t_env *env)
{
	t_pixel p;

	(void)x;
	(void)y;
	p.x = 0;
	p.y = 0;
	p.color = 0x000000;
	if (env->mandelbrot)
		mandelbrot_fractal(env, &p);
	else if (env->julia)
		julia_fractal(env, &p);
	else if (env->celtic)
		celtic_fractal(env, &p);
	else if (env->julia_celtic)
		julia_celtic_fractal(env, &p);
	else if (env->bird)
		bird_fractal(env, &p);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	return (0);
}

inline void		init_s_z(t_form *s, t_env *env)
{
	s->r = (600 + env->s.zoom) / (env->s.x2 - env->s.x1);
	s->i = (425 + env->s.zoom) / (env->s.y2 - env->s.y1);
}

int				mouse_hook(int button, int x, int y, t_env *env)
{
	static int	i;
	t_form		s;

	if (env->fractal_zoom)
	{
		if (button == 4 || button == 1)
		{
			if ((env->s.zoom *= 1.1) > 32785467297750543)
				env->s.zoom /= 1.1;
			i++;
		}
		else if (button == 2 || button == 3)
		{
			if ((env->s.zoom /= 1.1) <= 1)
				env->s.zoom = 1;
			i--;
		}
		if (i % 10 == 0)
			env->s.iter_max *= 1.1;
		init_s_z(&s, env);
		env->m.x = (x / s.r + env->m.x) - (300 / s.r);
		env->m.y = (y / s.i + env->m.y) - (212 / s.i);
		mouse_motion(x, y, env);
	}
	return (0);
}

int				mouse(int x, int y, t_env *env)
{
	if (env->motion)
	{
		prev_motion(x, y, env);
		mouse_motion(x, y, env);
	}
	return (0);
}
