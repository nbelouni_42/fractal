/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/15 08:49:01 by nbelouni          #+#    #+#             */
/*   Updated: 2014/12/17 01:21:00 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

int				check_arg(char *s, t_env *env)
{
	t_pixel	p;

	p.x = 0;
	p.y = 0;
	if (ft_strlen(s) == 2 && s[0] == '-')
	{
		if (s[1] == 'b')
			print_mandelbrot(env, &p);
		else if (s[1] == 'j')
			print_julia(env, &p);
		else if (s[1] == 'c')
			print_celtic(env, &p);
		else if (s[1] == 'x')
			print_julia_celtic(env, &p);
		else if (s[1] == 'p')
			print_bird(env, &p);
		else
		{
			print_info();
			return (1);
		}
		return (0);
	}
	print_info();
	return (1);
}
