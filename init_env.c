/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_env.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/15 04:34:06 by nbelouni          #+#    #+#             */
/*   Updated: 2014/12/16 08:42:14 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fract_ol.h"

int		init_env(t_env *e)
{
	if (!(e->mlx = mlx_init()))
		return (1);
	if (!(e->win = mlx_new_window(e->mlx, 600, 425, "fract_ol")))
		return (1);
	e->mandelbrot = 0;
	e->julia = 0;
	e->celtic = 0;
	e->julia_celtic = 0;
	e->bird = 0;
	e->motion = 0;
	e->fractal_zoom = 0;
	if (!(e->img = mlx_new_image(e->mlx, 600, 425)))
		return (1);
	return (0);
}
